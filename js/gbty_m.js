jQuery('html').removeClass('no-js').addClass('js');

$(window).load(function() {

    /* ==============================================
    Preloader
    =============================================== */
    function hidePreloader() {
            var loadingAnimation = $('#loader-animation'),
                preloader = $('#preloader');
    
            loadingAnimation.fadeOut();
            preloader.delay(preloaderDelay).fadeOut(preloaderFadeOutTime);
            $('#intro #iphone').animate({opacity: 1, marginTop: 0}, 1200);
            $('#intro #press').animate({opacity: 1, marginTop: 0}, 1200);
            $('#intro .get-started').animate({opacity: 1, marginTop: 20}, 1200);
           $('#product').animate({opacity: 1}, 1200);
           $('#product #iphone2').animate({opacity: 1, marginTop: -10}, 800);
           $('#product #photo').animate({opacity: 1, marginTop: 10}, 800);
           $('#product #screen').animate({opacity: 1, marginTop: 10}, 800);
            
        }
        
    var hasher = window.location.hash.substring(1);
    if (hasher == "product"){
    	var preloaderDelay = 350,
    	        preloaderFadeOutTime = 800;
    	$('#intro #iphone').css('opacity', 1);
    	$('#intro #press').css('opacity', 1);
    	$('#intro .get-started').css('opacity', 1);
    	$('#intro #iphone').css('margin-top', 0);
    	$('#intro #press').css('margin-top', 0);
    	$('#intro .get-started').css('margin-top', 0);
    	$('#main-nav').css('position','absolute');
    	hidePreloader();
    	
    }else{
    	var preloaderDelay = 350,
    	        preloaderFadeOutTime = 800;
    	
    	    
    		$('#product').css('opacity', 0);
    		$('#product #iphone2').css('opacity', 0);
    		$('#product #photo').css('opacity', 0);
    		$('#product #screen').css('opacity', 0);
    		$('#product #iphone2').css('margin-top', 90);
    		$('#product #photo').css('margin-top', 110);
    		$('#product #photo').css('float', 'left');
    		$('#product #photo').css('padding-right', 20);
    		$('#product #photo').css('display', 'inline-block');
    		$('#product #screen').css('margin-top',110);
    		$('#intro #iphone').css('opacity', 0);
    		$('#intro #press').css('opacity', 0);
    		$('#intro .get-started').css('opacity', 0);
    		$('#intro #iphone').css('margin-top', 100);
    		$('#intro #press').css('margin-top', 100);
    		$('#intro .get-started').css('margin-top', 100);
    	    hidePreloader();
    }
    
    
    

});

$(document).ready(function() {
	
    var wWidth = $(window).width(),
        mobileRes = 767;

    /* ==============================================
    Section Position
    =============================================== */
    function setSections() {
    	
        var sections = $("section"),
            wWidth = $(window).width(),
            wCounter = 0;
        
        if(wWidth > mobileRes) {  

            $.each(sections, function(eq) {
                if(eq > 0) {
                    $(this).css({'left': wCounter});
//                    $(this).css({'opacity': 0});
                }
                wCounter = wCounter + $(this).width();            
            }); 

        } else {
            $.each(sections, function(eq) {
                $(this).css({'left': 0});          
            }); 
        }     
    }

    function forcePosition() {
        var hash = window.location.hash,
        $panels = $('section');
        $panels.each(function(eq) {
            var panelId = $(this).attr('id');
            if( '#' + panelId == hash ) {
                var wWidth = $(window).width(),
                    scrollElement = 'html, body';

                $(scrollElement).stop().animate({
                    scrollLeft: wWidth * eq
                }, 300, 'easeOutCubic');
    
            }
        });
    }

    function resetWindowWidth() {
        wWidth = $(window).width();
    }

    $(window).resize(function() {
       // setSections();
        //forcePosition();
        //resetWindowWidth();
    });

    setSections();

    /* ==============================================
    Navigation
    =============================================== */
    var noIntro = $('body').hasClass('no-intro');

    function setNavigation() {
        var nav = $('nav#main-nav'),
            wWidth = $(window).width();
            
        if( !noIntro ) {
            if(wWidth > mobileRes) {
                nav.css({'left': 0 });
            }
        }
        
    }

    setNavigation();

    function adjustNavigation() {
        var nav = $('nav#main-nav'),
            scroll = $(window).scrollLeft(),
            wWidth = $(window).width();

        if( !noIntro ) {
            if(wWidth > mobileRes) {
               if(scroll >= wWidth) {
                    nav.css({
                        'left': 0
                    });
                } else {
                    nav.css({
                        'left': wWidth - scroll
                    });
                } 
            } else {
                nav.css({
                    'left': 0
                });
            }     
        }
        
    }

    $(window).scroll(function() {
     //   adjustNavigation();
    });

    $(window).resize(function() {
      //  adjustNavigation();
    });

    /* ==============================================
    Mobile Navigation
    =============================================== */

    $(function() {  
        var trigger = $('#responsive-nav');  
            menu = $('#main-nav ul');
      
        $(trigger).on('click', function(e) {  
            e.preventDefault();  
            menu.slideToggle();
            $(this).toggleClass('nav-visible');
        }); 

        $(window).resize(function(){  
            var windowW = $(window).width();  
            if(windowW > mobileRes && menu.is(':hidden')) {  
                menu.removeAttr('style');
            }  
        }); 
    });  

    /* ==============================================
    Smooth Scrolling
    =============================================== */
    var scrollElement = 'html, body',
        $scrollElement;
//    var fadeElement = 'section',
//        $fadeElement;

    $(function() {

        $('html, body').each(function () {
            if(wWidth > mobileRes) {
                var initScrollLeft = $(this).attr('scrollLeft');
            
                $(this).attr('scrollLeft', initScrollLeft + 1);
                if ($(this).attr('scrollLeft') == initScrollLeft + 1) {
                    scrollElement = this.nodeName.toLowerCase();
                    $(this).attr('scrollLeft', initScrollLeft);
                    return false;
                }
            }
                
        });
        $scrollElement = $(scrollElement);
    });

    $(function() {
        var $sections = $('section.section');  

        $sections.each(function() {
            var $section = $(this);
            var hash = '#' + this.id;
			
            $('a[href="' + hash + '"]').click(function(event) {
                moving = true;
                if(wWidth > mobileRes) {
                    $scrollElement.stop().animate({
                        scrollLeft: $section.offset().left
                    }, 1200, 'easeOutCubic', function() {
                        window.location.hash = hash;
						
						$('#product').animate({opacity: 1}, 1200);
						$('#product #iphone2').animate({opacity: 1, marginTop: -10}, 800);
						$('#product #photo').animate({opacity: 1, marginTop: 10}, 800);
						$('#product #screen').animate({opacity: 1, marginTop: 10}, 800);
//						$('#product #iphone2').animate({marginTop: -10}, 1200);
//						$('#product #photo').animate({marginTop: 10}, 1200);
//						$('#product #screen').animate({marginTop: 10}, 1200);
						
						
						
                    });
                   
                    
//                    alert("helloe");
//                    $('#product').animate({opacity: 'show'}, 30000);
					
                } else {
                    $scrollElement.stop().animate({
                        scrollTop: $section.offset().top
                    }, 1200, 'easeOutCubic');
                   $('#product').animate({opacity: 1}, 1200);
                   $('#product #iphone2').animate({opacity: 1, marginTop: -10}, 800);
                   $('#product #photo').animate({opacity: 1, marginTop: 10}, 800);
                   $('#product #screen').animate({opacity: 1, marginTop: 10}, 800);
                   
                }
                $('nav#main-nav a').removeClass('active');
                if($(this).hasClass('content-menu-link')) {
                    var link = $(this).attr('href');
                    $('a[href="' + hash + '"]').addClass('active');
                } else {
                    $(this).addClass('active');
                }

                var trigger = $('#responsive-nav'),
                    menu = $('#main-nav ul'); 

                if(trigger.hasClass('nav-visible')) {
                    menu.slideToggle();
                    trigger.toggleClass('nav-visible');
                }

                event.preventDefault();
            });
        });

    });

    function setInitActiveMenu() {
        var hash = window.location.hash;
        $('a[href="' + hash + '"]').addClass('active');
    }

    setInitActiveMenu();

    /* ==============================================
    Mobile Menu
    =============================================== */
    if ($('.mobile-nav').length && $('.mobile-nav').attr('data-autogenerate') == 'true') {
        var mobileMenu = $('.mobile-nav');
        $('ul.nav li a').each(function(index, elem) {
            mobileMenu.append($('<option></option>').val($(elem).attr('href')).html($(elem).html()));
        });
    }

    $('.mobile-nav').on('change', function() {
        link = $(this).val();
        if (!link) {
            return;
        }

        if (link.substring(0,1) == '#') {
            $('html, body').animate({scrollTop: $(link).offset().top - 74}, 750);
        } else {
            document.location.href = link;
        }
    });

    /* ==============================================
    Fancybox
    =============================================== */
    function bindFancybox() {
        $("a.fancybox").fancybox({
            'overlayShow'   : false,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
    }

    bindFancybox();

    /* ==============================================
    Input Placeholder for IE
    =============================================== */

    if(!Modernizr.input.placeholder){

        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
            var input = $(this);
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        }).blur();
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            });
        });

    }


    /* ==============================================
    Form validation
    =============================================== */
    
    var $contactForm = $('.contact-form'),
        formErrorClass = 'form-error',
        $responseMessage = $('.response-message');

    $.validator.setDefaults({
        submitHandler: function() {

            $.ajax({
                type: $contactForm.attr('method'),
                url: $contactForm.attr('action'),
                data: $contactForm.serialize(),
                success: function(data) {
                    $responseMessage.html(data);
                }
             });

        },
        highlight: function(input) {
                $(input).addClass(formErrorClass);
        },
        unhighlight: function(input) {
                $(input).removeClass(formErrorClass);
        }
    });

    jQuery($contactForm).validate({
        messages: {
            name: {
                required: ''
            },
            email: '',
            message: '',
            check: ''
        }
    });

});